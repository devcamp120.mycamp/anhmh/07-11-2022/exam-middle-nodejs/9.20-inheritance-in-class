import { Pupil } from "./pupil.js";
//Tạo một class Student kế thừa của class Pupil
class Student extends Pupil {
    //Constructor của class Student
    constructor(paramMajor, paramStudentCode) {
        super(paramMajor, paramStudentCode)
        this.major = paramMajor;
        this.studentCode = paramStudentCode;
    }
    //Phương thức của class con 
    print() {
        return this.major + " " + this.studentCode
    }
}
export { Student }