import { Human } from "./human.js";
//Tạo một class Pupil kế thừa của class Human
class Pupil extends Human {
    //Contructor của class Pupil
    constructor(paramSchoolName, paramClass, paramMobile) {
        super(paramSchoolName, paramClass, paramMobile)
        this.schoolName = paramSchoolName;
        this.class = paramClass;
        this.mobile = paramMobile
    }
    //Phương thức của Class con
    print() {
        return this.schoolName +  " " +  this.class + " " +  this.mobile;
    }
}
export { Pupil }