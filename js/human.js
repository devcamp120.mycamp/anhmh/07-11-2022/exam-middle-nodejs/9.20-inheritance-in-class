//Tạo một class Human (Con người)
class Human {
    //Hàm tạo Constructor
    constructor(paramFullName, paramDateOfBirth, paramPlaceOfOrigin) {
        this.fullName = paramFullName;
        this.dateOfBirth = paramDateOfBirth;
        this.placeOfOrigin = paramPlaceOfOrigin;
    }
    //Phương thức của Class
    print() {
        return this.fullName + " " + this.dateOfBirth + " " + this.placeOfOrigin + " "
    }
}
export { Human }