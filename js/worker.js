import { Human } from "./human.js";
//Tạo một class Worker kế thừa của class Human
class Worker extends Human {
    //Constructor của class Worker
    constructor(paramJobCategory, paramWorkLocation, paramSalary) {
        super(paramJobCategory, paramWorkLocation, paramSalary)
        this.jobCategory = paramJobCategory;
        this.workLocation = paramWorkLocation;
        this.salary = paramSalary;
    }
    //Phương thức của class con
    print() {
        return this.jobCategory + " " + this.workLocation + " " + this.salary;
    }
}
export { Worker }
