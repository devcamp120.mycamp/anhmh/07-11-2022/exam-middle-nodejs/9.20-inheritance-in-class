//Kế thừa trong class: các class trong ES6 giúp tạo các đối tượng,  thực hiện kế thừa dễ dàng hơn bằng cách sử dụng từ khoá extends và tái sử dụng code
import { Human } from "./js/human.js";
import { Pupil } from "./js/pupil.js";
import { Student } from "./js/student.js";
import { Worker } from "./js/worker.js";

//Khởi tạo đối tượng thể hiện class Human
var human_1 = new Human("Tony Robins", "13/03/1980", "United State");
console.log("Human 1: " + human_1.print());
//Kiểm tra xem đối tượng vừa tạo có kiểu đúng với class không
console.log(human_1 instanceof Human);

//Khởi tạo đối tượng thể hiện class Pupil
var pupil_1 = new Pupil("UCLA", "2000", "012345678");
console.log("Pupil 1: " + pupil_1.print());
//Kiểm tra xem đối tượng vừa tạo có kiểu đúng với class Pupil không
console.log(pupil_1 instanceof Pupil);

//Khởi tạo đối tượng thể hiện class Student
var student_1 = new Student("Public Relation", "UCLA50");
console.log("Student 1: " + student_1.print());
//Kiểm tra xem đối tượng vừa tạo có kiểu đúng với class Student không
console.log(student_1 instanceof Student)

//Khởi tạo đối tượng thể hiện class Worker
var worker_1 = new Worker("Plumber", "SanJose", "90000");
console.log("Worker 1: " + worker_1.print());
//Kiểm tra xem đối tượng vừa tạo có kiểu đúng với class Worker không
console.log(worker_1 instanceof Worker)
